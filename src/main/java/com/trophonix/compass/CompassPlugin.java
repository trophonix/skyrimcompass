package com.trophonix.compass;

import com.trophonix.compass.commands.LandmarksCommand;
import com.trophonix.compass.commands.CompassCommand;
import com.trophonix.compass.config.Config;
import com.trophonix.compass.data.DataHandler;
import com.trophonix.compass.listener.PlayerListener;
import lombok.Getter;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class CompassPlugin extends JavaPlugin {

  private Config conf;
  private DataHandler data;

  @Override public void onEnable() {
    saveDefaultConfig();
    conf = new Config(this, getConfig());
    try {
      conf.load();
    } catch (IllegalArgumentException ex) {
      getLogger().warning("Failed to load. " + ex.getMessage());
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
    new PlayerListener(this);

    CompassCommand compassCommand = new CompassCommand(this);
    PluginCommand skyrimCompassCommandBukkit = getCommand("compass");
    skyrimCompassCommandBukkit.setExecutor(compassCommand);
    skyrimCompassCommandBukkit.setTabCompleter(compassCommand);

    LandmarksCommand landmarksCommand = new LandmarksCommand(this);
    PluginCommand landmarksCommandBukkit = getCommand("landmarks");
    landmarksCommandBukkit.setExecutor(landmarksCommand);
    landmarksCommandBukkit.setTabCompleter(landmarksCommand);

    data = new DataHandler(this);
  }

}
