package com.trophonix.compass.data;

import com.trophonix.compass.CompassPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataHandler implements Listener {

  private CompassPlugin pl;

  private Set<UserData> users = new HashSet<>();

  public DataHandler(CompassPlugin pl) {
    this.pl = pl;
    pl.getServer().getPluginManager().registerEvents(this, pl);
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    users.add(new UserData(pl, event.getPlayer()));
  }

  @EventHandler
  public void onQuit(PlayerQuitEvent event) {
    users.removeIf(u -> u.getUuid().equals(event.getPlayer().getUniqueId()));
  }

  public UserData userData(Player player) {
    return users.stream().filter(u -> u.getUuid().equals(player.getUniqueId())).findFirst().get();
  }

}
