package com.trophonix.compass.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.trophonix.compass.CompassPlugin;
import com.trophonix.compass.Landmark;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UserData {

  private CompassPlugin pl;

  @Getter private UUID uuid;
  @Getter private Set<Landmark> landmarks;

  public UserData(CompassPlugin pl, Player player) {
    this.pl = pl;
    uuid = player.getUniqueId();
    landmarks = new HashSet<>();
  }

}
