package com.trophonix.compass.utils;

public class MathUtils {

  public static double min(double... ints) {
    if (ints.length == 0) return -1;
    if (ints.length == 1) return ints[0];
    double min = ints[0];
    for (int i = 1; i < ints.length; i++) {
      if (ints[i] < min) min = ints[i];
    }
    return min;
  }

}
