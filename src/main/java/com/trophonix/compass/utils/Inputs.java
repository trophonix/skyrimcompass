package com.trophonix.compass.utils;

import com.google.common.base.Preconditions;

public class Inputs {

  public static Number verifyNumber(String input, String errorMessage, Object... args) {
    try {
      return Double.parseDouble(input);
    } catch (NumberFormatException ex) {
      throw new IllegalArgumentException(String.format(errorMessage, args), ex);
    }
  }

  public static String verifyLength(String input, int length, String errorMessage) {
    Preconditions.checkArgument(input != null && input.length() == length, errorMessage, input);
    return input;
  }

}
