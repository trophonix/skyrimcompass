package com.trophonix.compass.config;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;

public class ConfigMessage {

  private String[] message;

  public ConfigMessage(ConfigurationSection config, String key) {
    if (config.isString(key)) {
      message = new String[]{ChatColor.translateAlternateColorCodes(
          '&', config.getString(key))};
    } else {
      message = config.getStringList(key).stream()
          .map(str -> ChatColor.translateAlternateColorCodes('&', str))
          .toArray(String[]::new);
    }
  }
  
  public void send(CommandSender sender, Object... args) {
    for (String ln : message) {
      for (int i = 0; i < args.length - 1; i += 2) {
        ln = ln.replace(args[i].toString(), args[i + 1].toString());
      }
      sender.sendMessage(ln);
    }
  }

}
