package com.trophonix.compass.config;

import com.trophonix.compass.CompassPlugin;
import com.trophonix.compass.Landmark;
import com.trophonix.compass.menu.ItemBuilder;
import com.trophonix.compass.utils.Inputs;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor @Getter
public class Config {

  private final CompassPlugin pl;
  private final ConfigurationSection config;

  private short updateSpeed;

  private String format;

  private char lineChar;
  private String lineFormat;

  private String pointerFormat;

  private boolean prioritizeLandmarks;
  private Set<Landmark> landmarks;

  private String landmarkTitle;

  private ItemBuilder landmarkIcon;
  private ItemBuilder landmarksMove;
  private ItemBuilder landmarksWorld;
  private ItemBuilder landmarksCharacter;
  private ItemBuilder landmarksColor;
  private ItemBuilder landmarksRemove;

  private ConfigMessage compassHelp;
  private ConfigMessage compassReloadError;
  private ConfigMessage compassReloaded;

  public void load() {
    updateSpeed = (short)config.getInt("updateSpeed", 1);
    format = config.getString("format");
    lineChar = Inputs.verifyLength(config.getString("lineChar"), 1, "Invalid char %s. Must be ONE character.").charAt(0);
    format = format.replace('-', lineChar);
    lineFormat = config.getString("lineFormat");
    pointerFormat = config.getString("pointerFormat");
    prioritizeLandmarks = config.getBoolean("prioritizeLandmarks");

    landmarks = new HashSet<>();
    ConfigurationSection landmarkSection = config.getConfigurationSection("landmarks");
    if (landmarkSection != null) {
      for (String key : landmarkSection.getKeys(false)) {
        try {
          landmarks.add(new Landmark(landmarkSection, key));
        } catch (IllegalArgumentException ex) {
          pl.getLogger().warning(String.format("Failed to load landmark %s. " + ex.getMessage(), key));
        }
      }
    }

    landmarkTitle = ChatColor.translateAlternateColorCodes('&', config.getString("lang.landmarks.title"));

    landmarkIcon = new ItemBuilder(Material.GOLD_NUGGET).from(config.getConfigurationSection("lang.landmarks.icon"));
    landmarksMove = new ItemBuilder(Material.COMPASS).from(config.getConfigurationSection("lang.landmarks.move"));
    landmarksWorld = new ItemBuilder(Material.MAP).from(config.getConfigurationSection("lang.landmarks.world"));
    landmarksCharacter = new ItemBuilder(Material.GOLD_NUGGET).from(config.getConfigurationSection("lang.landmarks.character"));
    landmarksColor = new ItemBuilder(Material.WOOL).from(config.getConfigurationSection("lang.landmarks.color"));
    landmarksRemove = new ItemBuilder(Material.BARRIER).from(config.getConfigurationSection("lang.landmarks.remove"));

    compassHelp = new ConfigMessage(config, "lang.compass.help");
    compassReloadError = new ConfigMessage(config, "lang.compass.reloadError");
    compassReloaded = new ConfigMessage(config, "lang.compass.reloaded");
  }

}
