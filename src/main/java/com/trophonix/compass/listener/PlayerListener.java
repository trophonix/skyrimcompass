package com.trophonix.compass.listener;

import com.trophonix.compass.CompassPlugin;
import com.trophonix.compass.Landmark;
import com.trophonix.compass.utils.MathUtils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;

public class PlayerListener implements Listener {

  /*

    DIRECTION REFERENCE

    +Z : SOUTH : 0
    -X : WEST : 90
    -Z: NORTH : 180
    +X : EAST : 270

 */

  public PlayerListener(CompassPlugin pl) {
    pl.getServer().getScheduler().runTaskTimer(pl, () -> {
      pl.getServer().getOnlinePlayers().forEach(player -> {
        if (!player.isDead()) {
          String format = pl.getConf().getFormat();
          char[] split = format.toCharArray();
          int increments = 360 / split.length;

          Map<Integer, Landmark> landmarks = new HashMap<>();
          if (pl.getConf().getLandmarks() != null) for (Landmark landmark : pl.getConf().getLandmarks()) {
            if (landmark.getWorld() == null || landmark.getWorld().equals(player.getWorld())) {
              double x1 = player.getLocation().getX(), z1 = player.getLocation().getZ();
              double x2 = landmark.getLocation().getX(), z2 = landmark.getLocation().getZ();
              if (x1 <= 0 || x2 <= 0 || z1 <= 0 || z2 <= 0) {
                double toAdd = Math.abs(MathUtils.min(x1, x2, z1, z2) - 1);
                x1 += toAdd;
                x2 += toAdd;
                z1 += toAdd;
                z2 += toAdd;
              }
              double a = Math.toDegrees(Math.atan2(z2 - z1, x2 - x1)) - 85;
              if (a < 0) a += 360;
              if (a >= 360) a -= 360;
              int idx = (int)Math.round(a) / increments;
              landmarks.put(idx, landmark);
            }
          }

          int angle = Math.round(player.getEyeLocation().getYaw() + 5f);
          if (angle < 0) angle += 360;
          if (angle > 360) angle -= 360;

          int facingIdx = angle / increments;
          int startIdx = facingIdx - (split.length / 4);
          int endIdx = facingIdx + ((split.length / 4) + 1);

          StringBuilder view = new StringBuilder(pl.getConf().getLineFormat());
          for (int i = startIdx * increments; i < endIdx * increments; i += increments) {
            int charIdx = i / increments;
            if (charIdx < 0) charIdx += split.length;
            if (charIdx >= split.length) charIdx -= split.length;

            char c = split[charIdx];
            if (charIdx == facingIdx) view.append(pl.getConf().getPointerFormat());
            if (pl.getConf().isPrioritizeLandmarks() || c == pl.getConf().getLineChar()) {
              Landmark landmark = landmarks.get(charIdx);
              if (landmark != null) {
                if (landmark.getFormat() != null) {
                  view.append(landmark.getFormat());
                }
                view.append(landmark.getCharacter());
                view.append(pl.getConf().getLineFormat());
                continue;
              }
            }
            view.append(c);
            if (charIdx == facingIdx) view.append(pl.getConf().getLineFormat());
          }

          player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(
              ChatColor.translateAlternateColorCodes('&', view.toString())));
        }
      });
    }, 0L, pl.getConf().getUpdateSpeed());
  }

}
