package com.trophonix.compass.commands;

import com.trophonix.compass.CompassPlugin;
import lombok.AllArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public class CompassCommand implements TabExecutor {

  private CompassPlugin pl;

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length > 0) {
      switch (args[0].toLowerCase()) {
        case "reload":
        case "rl":
          try {
            pl.getConf().load();
          } catch (IllegalArgumentException ex) {
            pl.getLogger().warning("Failed to load. " + ex.getMessage());
            pl.getServer().getPluginManager().disablePlugin(pl);
            pl.getConf().getCompassReloadError().send(sender);
            return true;
          }
          pl.getConf().getCompassReloaded().send(sender);
          return true;
      }
    }
    pl.getConf().getCompassHelp().send(sender);
    return true;
  }

  private static final List<String> arg0 = Arrays.asList("rl");

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
    return arg0;
  }
}
