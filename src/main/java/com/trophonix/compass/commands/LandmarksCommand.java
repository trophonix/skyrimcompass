package com.trophonix.compass.commands;

import com.trophonix.compass.CompassPlugin;
import com.trophonix.compass.Landmark;
import com.trophonix.compass.data.DataHandler;
import com.trophonix.compass.data.UserData;
import com.trophonix.compass.menu.LandmarksMenu;
import lombok.AllArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

@AllArgsConstructor
public class LandmarksCommand implements TabExecutor {

  private CompassPlugin pl;

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(ChatColor.RED + "You can't use landmarks.");
      return true;
    }
    Player player = (Player) sender;
    UserData user = pl.getData().userData(player);
    LandmarksMenu menu = new LandmarksMenu(pl, player, user);
    menu.loadButtons();
    menu.open();
    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
    return null;
  }

}
