package com.trophonix.compass.menu;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ItemBuilder {

  private final Material material;

  private String displayName;
  private List<String> lore;

  public ItemStack build() {
    ItemStack stack = new ItemStack(material);
    ItemMeta meta = stack.getItemMeta();
    if (meta != null) {
      meta.setDisplayName(displayName);
      meta.setLore(lore);
    }
    stack.setItemMeta(meta);
    return stack;
  }

  public ItemBuilder from(ConfigurationSection config) {
    displayName(config.getString("displayName"));
    lore(config.getStringList("lore"));
    return this;
  }

  public ItemBuilder replace(String from, Object to) {
    Optional<Object> opt = Optional.ofNullable(to);
    if (displayName != null) {
      displayName = displayName.replace(from, opt.orElse("").toString());
    }
    if (lore != null) {
      lore = lore.stream().map(str -> str.replace(from, opt.orElse("").toString())).collect(Collectors.toList());
    }
    return this;
  }

  public ItemBuilder displayName(String displayName) {
    if (displayName != null) {
      displayName = ChatColor.translateAlternateColorCodes('&', displayName);
    }
    this.displayName = displayName;
    return this;
  }

  public ItemBuilder lore(List<String> lore) {
    if (lore != null && !lore.isEmpty()) {
      lore = lore.stream().map(str -> ChatColor.translateAlternateColorCodes('&', str))
          .collect(Collectors.toList());
    }
    this.lore = lore;
    return this;
  }

}
