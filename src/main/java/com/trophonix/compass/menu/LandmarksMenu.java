package com.trophonix.compass.menu;

import com.trophonix.compass.CompassPlugin;
import com.trophonix.compass.Landmark;
import com.trophonix.compass.data.UserData;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

public class LandmarksMenu implements Listener {

  private static final int MENU_SIZE = 36;
  private static final int PREVIOUS_PAGE_SLOT = MENU_SIZE - 9;
  private static final int NEXT_PAGE_SLOT = MENU_SIZE - 2;
  private static final int NEW_SLOT = MENU_SIZE - 5;

  private final CompassPlugin pl;
  @Getter private final Player player;
  @Getter private final UserData user;

  private Inventory menu;
  private int page = 0;

  private Landmark[] landmarks;

  public LandmarksMenu(CompassPlugin pl, Player player, UserData user) {
    this.pl = pl;
    this.player = player;
    this.user = user;
    this.menu = Bukkit.createInventory(null, MENU_SIZE, pl.getConf().getLandmarkTitle());
    pl.getServer().getPluginManager().registerEvents(this, pl);
  }

  public void loadButtons() {
    int idx = (page * (MENU_SIZE - 9));
    landmarks = new Landmark[MENU_SIZE - 9];
    int i = 0;
    for (Landmark landmark : user.getLandmarks()) {
      if (i >= landmarks.length) break;
      landmarks[i++] = landmark;
    }

    Landmark landmark;
    for (int slot = 0; slot < MENU_SIZE - 9 && (landmark = landmarks[idx++]) != null; slot++) {
      menu.setItem(slot, landmark.replaceIn(pl.getConf().getLandmarkIcon()
      ).replace("{format}", landmark.getFormat()).build());
    }

    if (page > 0) {
      menu.setItem(PREVIOUS_PAGE_SLOT, new ItemBuilder(Material.ARROW)
          .displayName("&a[<]").build());
    }

    if (user.getLandmarks().size() >= idx) {
      menu.setItem(NEXT_PAGE_SLOT, new ItemBuilder(Material.ARROW)
          .displayName("&a[>]").build());
    }

    menu.setItem(NEW_SLOT, new ItemBuilder(Material.MAP)
      .displayName("&a[+]").build());
  }

  public void open() {
    player.openInventory(menu);
  }

  @EventHandler
  public void onClick(InventoryClickEvent event) {
    if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;
    if (event.getClickedInventory() == menu) {
      event.setCancelled(true);
      if (event.getSlot() < landmarks.length) {
        Landmark landmark = landmarks[event.getSlot()];

      } else {
        if (event.getSlot() == PREVIOUS_PAGE_SLOT) page--;
        else if (event.getSlot() == NEXT_PAGE_SLOT) page++;
        else if (event.getSlot() == NEW_SLOT) {

        } else return;
        loadButtons();
      }
    }
  }

  @EventHandler
  public void onClose(InventoryCloseEvent event) {
    if (event.getInventory() == menu) {
      HandlerList.unregisterAll(this);
    }
  }

}
