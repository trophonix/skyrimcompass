package com.trophonix.compass;

import com.google.common.base.Preconditions;
import com.trophonix.compass.menu.ItemBuilder;
import com.trophonix.compass.utils.Inputs;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

@Getter @Setter @AllArgsConstructor
public class Landmark {

  private String id;
  @Setter private World world;
  @Setter private Location location;
  @Setter private Character character;
  private String format;
  @Setter private Waypoint waypoint;

  public Landmark(ConfigurationSection config, String key) {
    id = key;
    if (config.isString(key)) {
      processLocation(config.getString(key));
    } else {
      ConfigurationSection section = config.getConfigurationSection(key);
      if (section.isString("world")) world = Bukkit.getWorld(section.getString("world"));
      processLocation(section.getString("location"));
      if (section.isString("character")) {
        character = Inputs.verifyLength(section.getString("character"), 1, "Invalid char %s. Must be ONE character.").charAt(0);
      }
      if (section.isString("format")) format = section.getString("format");
    }
  }

  public void save(ConfigurationSection config) {
    if (world == null && character == null && format == null) {
      config.set(id, toString(location));
    } else {
      ConfigurationSection section = config.createSection(id);
      if (world != null) section.set("world", world.getName());
      section.set("location", toString(location));
      if (character != null) section.set("character", character.toString());
      section.set("format", format);
      if (section.isConfigurationSection("waypoint"))
        waypoint = new Waypoint(section.getBoolean("waypoint.visible"),
          Color.fromRGB(section.getInt("waypoint.color")));
    }
  }

  private void processLocation(String string) {
    String[] split = string.split(" ");
    Preconditions.checkArgument(split.length > 2 && split.length < 6);
    double[] coords = new double[split.length];
    for (int i = 0; i < split.length; i++) {
      coords[i] = Inputs.verifyNumber(split[i], "Invalid location %s. Should be formatted as 'x y z [yaw (pitch)]'").doubleValue();
    }
    location = new Location(null, coords[0], coords[1], coords[2], coords.length > 3 ? (float)coords[3] : 0f, coords.length > 4 ? (float)coords[4] : 0f);
  }

  public ItemBuilder replaceIn(ItemBuilder item) {
    item.replace("{landmark}", id);
    item.replace("{world}", world != null ? world.getName() : "all");
    item.replace("{location}", toString(location));
    return item.replace("{color}", waypoint != null ? waypoint.color : "n/a");
  }

  private static String toString(Location location) {
    return String.format("%1$s %2$s %3$s", location.getX(), location.getY(), location.getZ());
  }

  @AllArgsConstructor
  public static class Waypoint {

    private boolean visible;
    private Color color;

  }

}
